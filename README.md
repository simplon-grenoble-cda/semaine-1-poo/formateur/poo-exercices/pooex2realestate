# Partie 1

On veut représenter dans notre application des logements.

Un logement a un loyer mensuel, une adresse postale. On veut aussi pouvoir savoir pour chaque logement
le loyer annuel, et si un logement est accessible pour une personne à mobilité réduite.

Il y a deux sous types de logement, des maisons et des appartements. Les appartements ont un numéro d'étage
(mais pas les maisons).

Une maison est toujours accessible pour les PMR. Un appartement est accessible pour les PMR seulement s'il est au rez de chaussée.
(Les ascenceurs n'ont pas été inventés).


# Partie 2 

On veut représenter des employés. Un employé a un salaire mensuel. On veut aussi connaitre le coût annuel d'un employé
(si on est son employeur), c'est à dire son salaire des 12 mois + les charges sociales qui sont 0.75 x le salaire (arrondi à l'euro près).

On veut représenter une entreprise. Une entreprise a un nom, et plusieurs employés. 
Elle loue également plusieurs habitations (pour ses bureaux).

On veut ensuite calculer le total des charges de l'entreprise sur une année (total des coût des employés + total des coût des habitations 
louées).


# Partie 3 

Simplifier notre code en mettant en place une interface.
