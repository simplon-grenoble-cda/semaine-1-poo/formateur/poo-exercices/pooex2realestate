package com.company;

public class Main {

    public static void main(String[] args) {

        //part1();

        //part2();


    }


    public static void part1() {
        House house1 = new House("9 boulevard Jean Moulin 38500 Voiron", 800);
        House house2 = new House("115 chemin du calvaire 42170 Saint Just Saint Rambert", 400);
        Flat flat1 = new Flat("14 rue songieu 69100 Villeurbanne", 200);
        Flat flat2 = new Flat("13 rue vauban 21000 Dijon", 500);

        List<Habitation> listOfHabitations = new ArrayList<Habitation>();
        listOfHabitations.add(house1);
        listOfHabitations.add(house2);
        listOfHabitations.add(flat1);
        listOfHabitations.add(flat2);

        RealEstateEvaluator.printEligibleHabitations(listOfHabitations);
        /* Expected :
             Habitation located at : 115 chemin du calvaire 42170 Saint Just Saint Rambert is eligible
             Habitation located at : 14 rue songieu 69100 Villeurbanne is eligible
         */
    }


    public static void part2() {
        Company simplonCompany = new Company("Simplon");
        Flat grenobleFlat = new Flat("34 Avenue de l'Europe Bâtiment D, 38100 Grenoble", 500);
        Flat lyonFlat = new Flat("34 Rue Antoine Primat, 69100 Villeurbanne", 800);
        simplonCompany.addPremise(grenobleFlat);
        simplonCompany.addPremise(lyonFlat);

        Employee louAnna = new Employee("Lou-Anna", 1500);
        Employee quentin = new Employee("Quentin", 1000);
        simplonCompany.addEmployee(louAnna);
        simplonCompany.addEmployee(quentin);

        System.out.println("Total annual cost of Simplon is : " + simplon.getTotalAnnualCost());
    }
}


